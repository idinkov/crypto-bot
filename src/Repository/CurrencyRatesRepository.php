<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\CurrencyRates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CurrencyRatesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CurrencyRates::class);
    }

    public function insertRate(Currency $currency, string $rateUsd, \DateTime $date)
    {

        $currencyRate = new CurrencyRates();
        $currencyRate->setCurrency($currency);
        $currencyRate->setDate($date);
        $currencyRate->setRateUsd($rateUsd);

        $em = $this->getEntityManager();
        $em->persist($currencyRate);
        $em->flush();

        return true;
    }
}
