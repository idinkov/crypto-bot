<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\Notification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    public function insertNotification(Currency $currency, string $text, \DateTime $date)
    {
        $notification = new Notification();
        $notification->setCurrency($currency);
        $notification->setDate($date);
        $notification->setText($text);

        $em = $this->getEntityManager();
        $em->persist($notification);
        $em->flush();

        return true;
    }
}
