<?php

namespace App\Utils\API;

use GuzzleHttp\Client;

class CoinmarketApi {

    private static function _call()
    {
        $client = new Client();
        $res = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/litecoin/');
        $body = $res->getBody();
        if (!$body) {
            return false;
        }

        $decode = json_decode($body, true);
        if (!$decode || !is_array($decode) || !isset($decode[0])) {
            return false;
        }

        return $decode[0];
    }

    public static function getLitecoinPriceUsd()
    {
        $result = self::_call();
        if (!isset($result['price_usd'])) {
            return false;
        }

        return $result['price_usd'];
    }

}