<?php

namespace App\Utils\API;

use GuzzleHttp\Client;

class CryptocompareApi {

    private static function _call($service, $params = false)
    {
        $client = new Client();
        $requestParams = '';
        if ($params) {
            $requestParams = "?" . http_build_query($params);
        }
        $res = $client->request('GET', "https://min-api.cryptocompare.com/data/{$service}{$requestParams}");
        $body = $res->getBody();
        if (!$body) {
            return false;
        }

        $decode = json_decode($body, true);
        if (!$decode || !is_array($decode) || !isset($decode)) {
            return false;
        }

        return $decode;
    }

    public static function getLitecoinPriceUsd()
    {
        $params = ['fsym' => 'LTC',
                   'tsyms' => 'USD'];

        $result = self::_call('price', $params);
        if (!isset($result['USD'])) {
            return false;
        }

        return $result['USD'];
    }
}