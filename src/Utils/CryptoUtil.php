<?php
namespace App\Utils;

use App\Entity\Currency;
use App\Entity\Notification;
use App\Repository\CurrencyRatesRepository;
use App\Repository\CurrencyRepository;
use App\Repository\NotificationRepository;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;

class CryptoUtil{

    /**
     * @var float
     */
    private $triggerBelow = ['LTC' => 214.50];

    const NOTIFICATON_COOLDOWN_MINUTES = 1;

    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * @var CurrencyRatesRepository
     */
    private $currencyRatesRepository;

    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    public function __construct(CurrencyRepository $currencyRepository, CurrencyRatesRepository $currencyRatesRepository, NotificationRepository $notificationRepository)
    {
        $this->currencyRepository = $currencyRepository;
        $this->currencyRatesRepository = $currencyRatesRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * @return Currency
     */
    private function _getLitecoinEntity()
    {
        return $this->currencyRepository->findOneByName('LTC');
    }

    public function getCoinbaseApi()
    {
        $apiKey = '';
        $apiSecret = '';
        $configuration = Configuration::apiKey($apiKey, $apiSecret);
        return Client::create($configuration);
    }

    public function getLitecoinRate()
    {

    }

    public function saveLitecoinRate(string $rate, \DateTime $date)
    {
        $currency = $this->_getLitecoinEntity();
        if (!$currency) {
            return false;
        }

        $rate = floatval($rate);
        if ($rate == 0) {
            return false;
        }

        return $this->currencyRatesRepository->insertRate($currency, $rate, $date);
    }

    public function checkLitecoinRateTriggerBelow(string $rate)
    {
        $currency = $this->_getLitecoinEntity();
        if (!$currency) {
            return false;
        }

        $rate = floatval($rate);
        if ($rate == 0) {
            return false;
        }

        if (!isset($this->triggerBelow['LTC'])) {
            return false;
        }

        $triggerBelow = $this->triggerBelow['LTC'];
        if ($triggerBelow == 0 || !$triggerBelow) {
            return false;
        }

        if ($triggerBelow > $rate) {
            return true;
        }

        return null;
    }

    private function _checkLitecoinNotificationStatus()
    {
        $currency = $this->_getLitecoinEntity();
        if (!$currency) {
            return false;
        }

        $lastNotification = $this->notificationRepository->findOneBy(['currency' => $currency], ['id' => 'DESC']);
        if ($lastNotification == null) {
            return true;
        }

        $now = new \DateTime();
        $lastNotificationDate = $lastNotification->getDate();
        $intervalMinutes = abs(($now->getTimestamp() - $lastNotificationDate->getTimestamp()) / 60);

        if ($intervalMinutes > self::NOTIFICATON_COOLDOWN_MINUTES) {
            return true;
        }

        return false;
    }

    public function notifyLitecoinRate(string $rate)
    {
        if (!$this->_checkLitecoinNotificationStatus()) {
            return null;
        }

        $currency = $this->_getLitecoinEntity();
        if (!$currency) {
            return false;
        }

        $text = "Litecoin is now {$rate}$! Please BUY!";

        $this->notificationRepository->insertNotification($currency, $text, new \DateTime());

        //Execute actual notification here
        $push = new \Pushover();
        $push->setToken('');
        $push->setUser('');
        $push->setTitle('LTC Alert');
        $push->setMessage($text);
        $push->setDevice('samsung_s7');
        $push->setTimestamp(time());
        $push->setDebug(true);
        $push->setSound('persistent');

        $go = $push->send();
        return true;
    }

}