<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171229215355 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE currency_rate CHANGE rate_usd rate_usd VARCHAR(255) NOT NULL, CHANGE rate_eur rate_eur VARCHAR(255) DEFAULT NULL, CHANGE rate_bgn rate_bgn VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE currency_rate CHANGE rate_usd rate_usd DOUBLE PRECISION NOT NULL, CHANGE rate_eur rate_eur DOUBLE PRECISION DEFAULT NULL, CHANGE rate_bgn rate_bgn DOUBLE PRECISION DEFAULT NULL');
    }
}
