<?php
namespace App\Command;

use App\Entity\Currency;
use App\Entity\CurrencyRates;
use App\Repository\CurrencyRepository;
use App\Utils\API\CoinmarketApi;
use App\Utils\API\CryptocompareApi;
use App\Utils\CryptoUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetLitecoinPriceCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('crypto:litecoin:price')

            // the short description shown while running "php bin/console list"
            ->setDescription('Get current litecoin price')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you fetch litecoin price...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo date('Y-m-d H:i:s') . " 1 LTC: ";
        $rateUsd = CoinmarketApi::getLitecoinPriceUsd();
        echo $rateUsd;
        $now = new \DateTime();

        $cryptoUtil = $this->getApplication()->getKernel()->getContainer()->get('crypto_util');
        if(!$cryptoUtil->saveLitecoinRate($rateUsd, $now)) {
            echo " - Invalid value";
            die();
        }

        // Check triggers
        $trigger = $cryptoUtil->checkLitecoinRateTriggerBelow($rateUsd);
        if ($trigger === null) {
            echo " - Trigger Not matched";
            die();
        }


        if (!$trigger) {
            echo " - No trigger defined";
            die();
        }

        echo " - Trigger [Matched]";

        $notification = $cryptoUtil->notifyLitecoinRate($rateUsd);
        echo " - Notification: ";

        if ($notification === null) {
            echo "[Too soon]";
            die();
        }

        if (!$notification) {
            echo "[Failed]";
            die();
        }

        echo "[Sent]";
    }
}