<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Table;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRatesRepository")
 * @ORM\Entity @Table(name="currency_rate")
 */
class CurrencyRates
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Currency", inversedBy="rates")
     * @JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="string")
     */
    private $rateUsd;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $rateEur;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $rateBgn;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getRateUsd()
    {
        return $this->rateUsd;
    }

    /**
     * @param mixed $rateUsd
     */
    public function setRateUsd($rateUsd)
    {
        $this->rateUsd = $rateUsd;
    }

    /**
     * @return mixed
     */
    public function getRateEur()
    {
        return $this->rateEur;
    }

    /**
     * @param mixed $rateEur
     */
    public function setRateEur($rateEur): void
    {
        $this->rateEur = $rateEur;
    }

    /**
     * @return mixed
     */
    public function getRateBgn()
    {
        return $this->rateBgn;
    }

    /**
     * @param mixed $rateBgn
     */
    public function setRateBgn($rateBgn): void
    {
        $this->rateBgn = $rateBgn;
    }
}
