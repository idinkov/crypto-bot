<?php
namespace App\Admin;

use App\Entity\Currency;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CurrencyRatesAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        // Implement Custom FormTypeInterface to select from dropdown
        $formMapper->add('currency', 'integer');
        $formMapper->add('date', 'datetime');
        $formMapper->add('rateUsd', 'text');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('currency', ArrayCollection::class);
        $datagridMapper->add('date');
        $datagridMapper->add('rateUsd');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('date');
        $listMapper->addIdentifier('rateUsd');
    }
}